## Executar aplicação no Docker

### Configuração banco MySQL
* Configure uma base de dados com o nome de 'chartbrew'.
* Atualize o arquivo .env-template com a informações do seu banco, por exemplo:
    * CB_DB_NAME=chartbrew
    * CB_DB_USERNAME=root
    * CB_DB_PASSWORD=

### Build Docker
```
docker build -t getting-started .
```
### Executar aplicação
```
docker run -dp 4018:4018 getting-started
```